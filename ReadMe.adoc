:author: Christian.sueur
:contact: contact@christiansueur.com

:toc:


toc::[]

= Linux_AutoInstallPack =

Le script *Linux_AutoInstallPack* permet de faire une installation rapide et
facile de plusieur logiciels tels que vim, htop, links ,.. sur une installation
fresh d'un linux.

.Installation en suivant 3 option
[source]
* Server = Logiciels uniquement ligne de commande
* Desktop Basic = Logiciel Server + Logiciel avec GUI
* Desktop Design = Logiciel Server + Logiciel Desktop Basic + Logiciel traitement d'images/sons/vidéo

.Liste des Logiciels
[source]
* vim
* htop
* git
* feh
* evince
* ranger
* links
* gimp
* inkscape
* kdenlive
* teamviewer (ask for the installation)
* simplescreenrecorder
* blender
